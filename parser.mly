%token <string> VAR
%token <int> INT
%token <bool> BOOL
%token EQUAL
%token PRINT
%token STORE
%token EOF
%token SEMICOLON
%token LPAR
%token RPAR
%token EMP
%token PLUS
%token DIV
%token SUB
%token MUL
%token WHILE
%token FOR
%token IF
%token SBRACKET
%token CBRACKET
%token PRINTN
%token NOT
%token AND
%token MAIN

%start <Program.program option> prog
%%

prog:
  | EOF         { None   } 
  | MAIN SBRACKET v = program CBRACKET { Some v }
;

expression:
  | i = INT { Number(i) }
  | x = VAR { Var(x) }
  | LPAR e1 = expression PLUS e2 = expression RPAR {Plus(e1,e2)}
  | LPAR e1 = expression MUL e2 = expression RPAR {Mul(e1,e2)}
  | LPAR e1 = expression DIV e2 = expression RPAR {Div(e1,e2)}
  | LPAR e1 = expression SUB e2 = expression RPAR {Sub(e1,e2)}
;

condition:
  | LPAR c1 = condition AND c2 = condition RPAR { And(c1,c2) }
  | NOT LPAR c = condition RPAR { Not(c) }
  | b = BOOL { Bool(b) }
  | e1 = expression EQUAL e2 = expression { Equal(e1, e2) }
; 

instruction:
  | PRINTN LPAR e = expression RPAR { Printn(e) }
  | PRINT LPAR v = VAR RPAR { Print(Var(v)) }
  | STORE LPAR v = VAR SEMICOLON e = expression RPAR { Store(Var(v), e) }
  | v = VAR STORE e = expression { Store(Var(v), e) }
  | v = VAR PLUS PLUS { Inc(Var(v)) }
  | v = VAR SUB SUB { Dec(Var(v)) }
;

program:
  | EMP { `Empty } 
  | i = instruction { `Instruction i }
  | WHILE LPAR c = condition RPAR LPAR p = program RPAR { `While(c, p)  }
  | IF LPAR c = condition RPAR LPAR p1 = program RPAR LPAR p2 = program RPAR  { `If (c, p1, p2) }
  | FOR LPAR i1 = instruction SEMICOLON c = condition SEMICOLON i2 = instruction
    RPAR LPAR p = program RPAR
    { `For(i1,c,i2,p) }
  | p1 = program SEMICOLON p2 = program { `Seq(p1, p2) }
  | p = program SEMICOLON { p }
;
