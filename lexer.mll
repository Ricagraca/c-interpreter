{
open Lexing
open Parser

exception SyntaxError of string

let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_bol = lexbuf.lex_curr_pos;
               pos_lnum = pos.pos_lnum + 1
    }
}

let int = ['0'-'9'] ['0'-'9']*
let digit = ['0'-'9']
let frac = '.' digit*
let exp = ['e' 'E'] ['-' '+']? digit+
let float = digit* frac? exp?
let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let id = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

rule read =
  parse
  | white    { read lexbuf }
  | newline  { next_line lexbuf; read lexbuf }
  | "print"  { PRINT }
  | "printn" { PRINTN }
  | "store"  { STORE }
  | ":="     { STORE }  
  | "main"   { MAIN }
  | "while"  { WHILE }
  | "for"  { FOR }  
  | "if"     { IF    }
  | "true"   { BOOL (bool_of_string (Lexing.lexeme lexbuf))}
  | "false"  { BOOL (bool_of_string (Lexing.lexeme lexbuf))}  
  | "()"     { EMP }
  | "!"      { NOT }
  | "&&"     { AND }
  | int      { INT (int_of_string (Lexing.lexeme lexbuf)) }
  | id       { VAR (Lexing.lexeme lexbuf) }
  | '{'      { SBRACKET }
  | '}'      { CBRACKET }
  | '('      { LPAR }
  | ')'      { RPAR }
  | '+'      { PLUS }
  | '-'      { SUB }
  | '/'      { DIV }
  | '*'      { MUL }
  | ';'      { SEMICOLON }
  | '='      { EQUAL }
  | _ { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
  | eof      { EOF }