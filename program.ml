
open Hashtbl
open Printf

let htbl = Hashtbl.create 10
let () = Hashtbl.add htbl "" 0

type variable =
  Var of string

type expression =
  | Var of string
  | Number of int
  | Plus of expression * expression
  | Mul  of expression * expression
  | Sub  of expression * expression
  | Div  of expression * expression

type condition =
  | And of condition * condition
  | Not of condition
  | Equal of expression * expression
  | Bool of bool

type instruction =
  | Printn of expression
  | Print of variable
  | Inc of variable
  | Dec of variable 
  | Store of variable * expression
  | Null 
                
type program = [
  | `Empty
  | `Instruction of instruction
  | `While of condition * program
  | `For of instruction * condition * instruction * program
  | `If of condition * program * program
  | `Seq of program * program
  ];;
          
let rec eval_expression expr =
  match expr with
  | Var(x) -> Hashtbl.find htbl x
  | Number(n) -> n
  | Plus(n1, n2) -> (eval_expression n1) + (eval_expression n2)
  | Mul(n1, n2)  -> (eval_expression n1) * (eval_expression n2)
  | Sub(n1, n2)  -> (eval_expression n1) - (eval_expression n2)
  | Div(n1, n2)  -> (eval_expression n1) / (eval_expression n2)

let rec eval_condition cond =
  match cond with
  | And(c1,c2)  -> (eval_condition c1) && (eval_condition c2)
  | Not(c)      -> not (eval_condition c)
  | Bool(b)     -> b
  | Equal(a, b) -> (eval_expression a) = (eval_expression b)
                  
let rec eval_program prog =
  match prog with
  | `Empty -> ()
  | `Instruction inst -> begin 
     match inst with
     | Print(Var(var)) -> printf "%d" (Hashtbl.find htbl var)
     | Printn(e) ->
        let evaled_expression =  eval_expression e in 
        printf "%d\n" (evaled_expression)
     | Store(Var(var), expr) ->
        let evaled_expression = eval_expression expr in
        Hashtbl.add htbl var (evaled_expression)
     | Inc(Var(v)) ->
         let evaled_expression = eval_expression (Var(v)) in
         Hashtbl.add htbl v (1 + evaled_expression)
     | Dec(Var(v)) ->
         let evaled_expression = eval_expression (Var(v)) in
         Hashtbl.add htbl v (evaled_expression - 1)
     | Null -> ()
    end                       
  | `While(cond, block) -> begin 
     let evaled_cond = eval_condition cond in
     match evaled_cond with
     | true  -> (eval_program block); eval_program prog ;
     | false -> ()
    end
  | `For(i1,c,i2,p) -> begin
      eval_program (`Instruction i1);
      let loop = eval_condition c in
      if loop then
        (
          eval_program (p);
          eval_program (`Instruction i2);
          eval_program (`For (Null,c,i2,p));
        )
      else ()
    end
  | `If(cond, prog1, prog2) -> begin 
      let evaled_cond = eval_condition cond in
      match evaled_cond with
      | true  -> eval_program prog1
      | false -> eval_program prog2
    end
  | `Seq(prog1, prog2) -> begin
      eval_program prog1; eval_program prog2
    end
;;

                 

